(function(root, doc, $){    
    $(document).ready(function(){
        
        var form = $('#simple-ajax-form'),
            contact = $('#contact'),
            formAction = 'simpleajaxcontact_send_mail';
            
            
       //Form handler
        form.on('submit', function(e){
            e.preventDefault();
            
            var inputs = {};
            $(this).find('.form-control').each(function(i,input){                
                inputs[this.name] = this.value;
            });
            ajaxformsendmail(inputs);
        });
        
        function ajaxformsendmail(values) {
                        
            $.ajax({                
                type: 'POST',
                url: sacf.ajaxurl,
                data: {
                    action: formAction,
                    name: values.name,
                    email: values.email,
                    company: values.company,
                    message: values.message,                    
                },
                beforeSend: function() {                    
                    //add overlay with spinner                    
                    var overlay = $("<div class='overlay'><div class='container'><div class='content'></div></div></div>")
                                   .css({
                                       height: $(window).innerHeight(),
                                       top: $(window).scrollTop()
                                    });                                   
                    overlay.find('.content').html('<i class="fa fa-cog fa-spin"></i>');
                    $('#contact').append(overlay);                    
                    $('.form-errors').addClass('hidden');
                    form.addClass('form-fade');
                    form.find('.form-control').removeClass('error');
                },
                success: function(response) {
                    var content = $('.overlay').find('.content');                  
                    
                    if(!response.success) {
                        //display error messages
                        $('.overlay').remove();
                        form.removeClass('form-fade');
                        
                        var errorText = '';
                        if(typeof response.data == 'string') {
                            errorText += '<p>'+ response.data.replace(/\s+/g, ' '); + '</p>';
                        } else {
                            for(input in response.data) {
                                form.find('.form-control[name="' + input + '"]').addClass('error');                            
                                errorText += '<p>'+ response.data[input] + '</p>';
                            }
                        }
                        $('.form-errors').html(errorText);
                        $('.form-errors').removeClass('hidden');
                        
                        return 0;
                    }
                    
                    //add success message to overlay                    
                    form.addClass('form-fade');
                    content.html('<i class="success fa fa-check"></i><p>Message Sent</p>');
                },
            });
        }
    });
    
})(window, document, jQuery);


