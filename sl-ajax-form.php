<?php

/*
 * Plugin Name: ShaunLewin.com AJAX Form Plugin
 * 
 * Plugin URI: http://www.shaunlewin.com
 * 
 * Description: Simple non-bloated WordPress Form using AJAX
 * 
 * Verson: 1.0
 * 
 * Author: Shaun Lewin
 * 
 * Author URI: http://www.shaunlewin.com
 * 
 */

defined('ABSPATH') or die('No direct script access allowed!');

define('SAC_NAME', 'simpleajaxcontact');
define('SAC_URL', WP_PLUGIN_URL . "/" . dirname(plugin_basename(__FILE__)));
define('SAC_PATH', WP_PLUGIN_DIR . "/" . dirname(plugin_basename(__FILE__)));
define('AJAX_DIR', get_template_directory_uri());

add_action('wp_enqueue_scripts', 'simpleajaxform_enqueuescripts');
add_action('wp_ajax_nopriv_simpleajaxcontact_send_mail', 'simpleajaxcontact_send_mail');
add_action('wp_ajax_simpleajaxcontact_send_mail', 'simpleajaxcontact_send_mail');

function get_javascript_vars() {
    return array(
        'ajaxurl' => AJAX_DIR . "/admin-ajax.php",
    );
}

function simpleajaxform_enqueuescripts() {
    wp_enqueue_script(SAC_NAME, SAC_URL . '/js/sl-ajax-form.js', array('jquery'));
    wp_enqueue_style('simple-ajax-form-css', SAC_URL . '/css/sl-ajax-form.css');
    wp_localize_script(SAC_NAME, 'sacf', get_javascript_vars());  
}

function simpleajaxform_enqueuestyles(){
    wp_enqueue_style(SAC_NAME, SAC_URL . '/css/custom-form.css');
}

function simpleajaxcontact_send_mail() {
    $success = false;
    $error = array();
    
    $name = sanitize_text_field($_POST['name']);
    $email = sanitize_email($_POST['email']);
    $company = sanitize_text_field($_POST['company']);
    $message = esc_textarea($_POST['message']);
    
    //Validate input
    if (strlen($name) == 0) {
        $error['name'] = "Name not valid!";
    }    
    if (!is_email($email) || strlen($email) == 0) {
        $error['email'] = 'Email address is invalid!';
    }    
    if (strlen($company) == 0) {
        $error['company'] = "Company name not valid!";
    }    
    if (strlen($message) == 0) {
        $error['message'] = "Message not valid!";
    }
    
    if(empty($error)) {
       $adminemail = get_option('admin_email');
       $headers = "From: " . $name . "<" . $email . ">";
              
       if (wp_mail($adminemail, 'ShaunLewin.com Contact Request', $message, $headers)) {
           $success = true;
           wp_send_json_success();
       } else {
           $error['email'] = "Email failed to send!";
       }
    }
    
    wp_send_json_error($error);
}

?>